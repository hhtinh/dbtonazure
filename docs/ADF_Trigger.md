# Trigger ACG by ADF

Dated : Dec-2020

Typically when it comes to data processing, ADF is a common solution that gets adopted for various data ingestion
and data processing pipelines. When it comes to triggering the ACG, a typical solution is to Logic App or Azure functions.
It would be much ease if the ACG can be integrated with ADF, so that all the code base are in the same place for
data ingestions and data processing etc.. Hence I am demonstrating how this can be achieved using ADF.

### Simulations
For the sake of a demo/workable prototype, i am simulating a wait time in the ACG by introducing a sleep for 5 minutes.
Source : [../dbtdataops/dbtoncloud/dbt_datapipeline.sh](../dbtdataops/dbtoncloud/dbt_datapipeline.sh)
![](./images/code_simulation_for_processing.png)

## Demo execution - ADF
In the below video walk through, i am demonstrating the ADF triggering the ACG. Once instantiated, I highlight : 
 - The service principal secrets that gets stored in the key vault
 - The Key vault linked service in the ADF
 - The pre-configured pipeline which is used to trigger the ACG
 - Show that the ACG was in a stopped state
 - Trigger, manually, the pipeline.
 - The pipeline triggers the ACG and waits 
 - ACG starts and performs its actions.
 - Once completed, show the log output of ACG and also the status in ADF.

[Video : ./videos/demo_execution_adf_tigger.mp4](./videos/demo_execution_adf_tigger.mp4)

## Design

### Service principal
We can start an instance of the ACG using REST API, as mentioned in the [Doc : Azure Container Instances REST API reference](https://docs.microsoft.com/en-us/rest/api/container-instances/containergroups). In order to do access this service, we need to get an access token. 

As of today, we cannot get a token for ADF managed identity and neither for the previous created user managed 
identity (hmuse1dbtazuai01). However we can use the service principal, which we used for the deployment to get 
an access token, as mentioned in [ Doc : How to use managed identities for Azure resources on an Azure VM to acquire an access token](https://docs.microsoft.com/en-us/azure/active-directory/managed-identities-azure-resources/how-to-use-vm-token#get-a-token-using-curl).

Hence to aid in this, the service principal id and its secrets gets stored in the key vault during deployment:
![](./images/kv_secrets.png)

### ADF

#### Linked service 
In order for the ADF to read the secrets from the key vault, it has been preconfigured
with a linked service and access policies where defined for the ADF to access the key vault.
![](./images/adf_kv_ls.png)

### Pipeline : acg_start_and_wait_pipe
A parameterized pipeline, acg_start_and_wait_pipe, is also preconfigured in the ADF
instance. I choose to have the parameterize the pipeline, so that it can be used in your adoption across multiple instances. 

![](./images/acg_start_and_wait_pipe.png)

The following are its parameter:
| Resource Name | Comment |
| :---------- | :--- |
| acg_name | Azure container group, which hosts the dbt container. |
| sleep_time_potential_completion | A rough estimate time for the data pipeline is expected to finish.Specify in seconds. |
| az_tenant_id | Azure tenant id. |
| az_sub_id | Azure subscription id. |
| acg_rg | The resource group which host the ACG. |


#### Activities : Get the access token
As mentioned above, we need to get the service principal and its secrets stored
in the key vault and use it to get an access token. These are observed by the 
following activities:
  - kv_get_acr-sp
  - kv_get_acr-sp-scrt
  - get_access_token

#### Activities : ACG Start and wait
As mentioned above, we can start the ACG using the rest api. Once the ACG starts,
the call returns immediately. Since transformation could run for some time, I have
introduced a wait activity. How long to wait, would depends on your amount of data
that gets processed etc. You would have to set this to some minutes that is more
appropriate to your adoption. The parameter, sleep_time_potential_completion, is
the value to set. 

These steps are observed by activity :
 - start_acg
 - Wait_for_acg_finish

#### Activities : Get status of ACG
To get the status of the ACG, you can use the [Doc : GET operation](https://docs.microsoft.com/en-us/rest/api/container-instances/containergroups/get). This is observed
by the activity :
 - get_acg_status

If the returned state is "Succeeded", it means that the ACG has finished processing.
![](./images/get_acg_status_output.png)

If the state is not sucessful, we would need to wait for some more time, I leave the implementation for this loop back for you to implement.

## Limitation

### Chaining the ACG invocation

It is only natural, for you to ask, if we can chain multiple ACG calls using the 
[Execute pipeline activity](https://docs.microsoft.com/en-us/azure/data-factory/control-flow-execute-pipeline-activity). Unfortunately, the current implementation
does not return any output other than the RunId. Azure had reported that they are
working on this feature. Until that time, the approach would not be straight forward
and would be complex.





