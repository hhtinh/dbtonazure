
resource "azurerm_container_registry" "acr" {
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  name = join("", [
    local.name_prefix,
    "acr01"
  ])

  tags = var.tags_base
  sku  = "Basic"
}

resource "azurerm_user_assigned_identity" "uai" {
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  name = join("", [
    local.name_prefix,
    "uai01"
  ])
}

# The user assigned identity should be able to access the
# storage account so that it can copy the project code
# into the aci.
resource "azurerm_role_assignment" "uai_ra" {
  scope                = azurerm_storage_account.adls2.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = azurerm_user_assigned_identity.uai.principal_id
}

resource "azurerm_role_assignment" "uai_ra_sakey" {
  scope                = azurerm_storage_account.adls2.id
  role_definition_name = "Storage Account Key Operator Service Role"
  principal_id         = azurerm_user_assigned_identity.uai.principal_id
}

# The user assigned identity should be able to read the
# key vault, which is where snowflake connectivity information
# is stored. 
resource "azurerm_key_vault_access_policy" "uai_kv" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azurerm_user_assigned_identity.uai.principal_id

  secret_permissions = ["get", "list", "set"]
}

#
# --------------------------------------------------------------------
#
# Build the dbt docker image, currently there is no tf provider definition
# hence we are using local exec.
#
resource "null_resource" "build_docker_dbt" {
  provisioner "local-exec" {
    command = <<BUILD_CMD_EOF
     az acr login --name $ACR

     echo "Building docker image remotely in $ACR ..."
      az acr build -t $DKR_TAG -r $ACR ../dbtdocker

    BUILD_CMD_EOF

    environment = {
      ACR     = azurerm_container_registry.acr.name
      DKR_TAG = var.dbt_docker_tag
    }

  }
}


